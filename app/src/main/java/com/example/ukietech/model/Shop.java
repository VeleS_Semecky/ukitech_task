package com.example.ukietech.model;

import java.util.List;

/**
 * Created by Юрий on 26.03.2018.
 */

public class Shop {
    private String nameShop;
    private List<Product> productList;

    public Shop(String nameShop) {
        this.nameShop = nameShop;
    }

    public Shop() {
    }

    public Shop(String nameShop, List<Product> productList) {
        this.nameShop = nameShop;
        this.productList = productList;
    }

    public String getNameShop() {
        return nameShop;
    }

    public void setNameShop(String nameShop) {
        this.nameShop = nameShop;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}
