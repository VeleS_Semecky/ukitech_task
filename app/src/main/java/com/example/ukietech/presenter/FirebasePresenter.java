package com.example.ukietech.presenter;

import com.example.ukietech.presenter.core.BasePresenter;

import java.util.prefs.Preferences;

public class FirebasePresenter extends BasePresenter<WorkWithFirebase.View> implements WorkWithFirebase.Presenter {

    private final Preferences preferences;

    public FirebasePresenter(Preferences preferences) {
        this.preferences = preferences;
    }

    @Override
    public void onAddNewShop() {
        getView().showAddNewShop();
    }

    @Override
    public void onFailedAdd() {

    }

    @Override
    public void onGoodAdd() {

    }

    @Override
    public void viewIsReady() {

    }
}
