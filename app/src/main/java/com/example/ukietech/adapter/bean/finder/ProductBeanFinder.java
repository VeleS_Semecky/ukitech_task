package com.example.ukietech.adapter.bean.finder;

import android.content.ContentResolver;
import android.content.res.Resources;
import android.net.Uri;
import android.util.Log;

import com.example.ukietech.R;
import com.example.ukietech.model.Product;
import com.example.ukietech.model.Shop;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@EBean
public class ProductBeanFinder {


//    @NonNull
//    public List<Shop> getListShops() {
//        return listShops;
//    }

    public void getShopsInFirebase(Resources resources ){

        Product product = new Product();
        DatabaseReference myRef;
        String SHOPS_ROOT = "Products";
        myRef = FirebaseDatabase.getInstance().getReference().child(SHOPS_ROOT);
        String key = myRef.push().getKey();
        Uri imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                "://" + resources.getResourcePackageName(R.drawable.qaz)
                + '/' + resources.getResourceTypeName(R.drawable.qaz)
                + '/' + resources.getResourceEntryName(R.drawable.qaz));
        StorageReference reference = FirebaseStorage.getInstance().getReference().child("image_feed/" + Calendar.getInstance().getTimeInMillis() + ".jpg");
        product.setNameProduct("Pepsi");
        product.setPriceProduct("$14");
        product.setQuantity(12);
        reference.putFile(imageUri).addOnSuccessListener(taskSnapshot -> {
            Uri fullSizeUrl = taskSnapshot.getDownloadUrl();
            assert fullSizeUrl != null;
            product.setUrlPhoto(fullSizeUrl.toString());
            myRef.child(key).setValue(product); });
    }
    public void sortList(List listProducts){
        Collections.sort(listProducts, (Comparator<Product>) (o1, o2) -> listProducts.indexOf(o2)- listProducts.indexOf(o1));
    }
}
