package com.example.ukietech.adapter.bean;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.ukietech.R;
import com.example.ukietech.adapter.product.ProductAdapter;
import com.example.ukietech.ui.activity.core.BaseActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.ViewById;
@EBean
public class ProviderBeanProduct {
    @RootContext
    BaseActivity baseActivity;

    @Bean
    ProductAdapter productAdapter;

    @ViewById(R.id.recycler_view)
    RecyclerView recyclerView;

    @AfterViews
    public void initProductAdapter(){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(baseActivity));
        recyclerView.setAdapter(productAdapter);
    }

    public void addNewProduct(){
        productAdapter.notifyDataSetChanged();
        recyclerView.scrollToPosition(0);
    }
}
