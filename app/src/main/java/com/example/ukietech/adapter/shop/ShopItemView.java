package com.example.ukietech.adapter.shop;

import android.content.Context;
import android.content.Intent;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.ukietech.R;
import com.example.ukietech.model.Shop;
import com.example.ukietech.ui.activity.ProductsActivity_;
import com.example.ukietech.ui.activity.core.BaseActivity;
import com.example.ukietech.ui.fragment.core.BaseFragment;
import com.example.ukietech.ui.navigator.NavigatorManager;
import com.example.ukietech.ui.navigator.core.ResourceManager;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.item_shop)
public class ShopItemView extends LinearLayout {

    @Bean
    protected NavigatorManager navigatorManager;

    @ViewById(R.id.item_shop)
    TextView item_shop;

    @Click(R.id.item_shop)
    public void OnClick(){
        navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.LIST_PRODUCT_FRAGMENT, item_shop.getText());
    }

    public ShopItemView(Context context) {
        super(context);
        this.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));

    }

    public void bind(Shop shop){
        item_shop.setText(shop.getNameShop());
    }

}
