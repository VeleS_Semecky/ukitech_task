package com.example.ukietech.adapter.bean;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import com.example.ukietech.R;
import com.example.ukietech.adapter.shop.ShopAdapter;
import com.example.ukietech.ui.activity.core.BaseActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.ViewById;

@EBean
public class ProviderBeanShop {

    @RootContext
    BaseActivity baseActivity;

    @Bean
    ShopAdapter shopAdapter;

    @ViewById(R.id.recycler_view)
    RecyclerView recyclerView;

    @AfterViews
    public void initShopAdapter(){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(baseActivity));
        recyclerView.setAdapter(shopAdapter);
    }
}
