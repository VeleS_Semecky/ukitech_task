package com.example.ukietech.adapter.core;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public abstract class RecyclerViewAdapterBase<T,V extends View> extends RecyclerView.Adapter<ViewWrapper<V>>{

    List<T> item = new ArrayList<>();

    public void setItem(List<T> item){
        this.item = item;
        notifyDataSetChanged();
    }

    public List<T> getItem() {
        return item;
    }

    @Override
    public int getItemCount() {
        return item == null ? 0 : item.size();
    }

    @Override
    public ViewWrapper<V> onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewWrapper<V>(onCreateItemHolder(parent,viewType));
    }

    protected abstract V onCreateItemHolder(ViewGroup parent, int viewType);
}
