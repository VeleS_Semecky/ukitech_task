package com.example.ukietech.adapter.product;

import android.content.Context;
import android.content.Intent;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ukietech.R;
import com.example.ukietech.model.Product;
import com.example.ukietech.ui.activity.ProductsActivity;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.item_product)
public class ProductItemView extends LinearLayout {

    @ViewById(R.id.textNameProduct)
    TextView textNameProduct;

    @ViewById(R.id.textQuantity)
    TextView textQuantity;

    @ViewById(R.id.textPriceProduct)
    TextView textPriceProduct;

    @ViewById(R.id.imageProduct)
    ImageView imageProduct;

    @Click(R.id.item_shop)
    public void OnClick(){

        Intent intent = new Intent(getContext(), ProductsActivity.class);
        getContext().startActivity(intent);
    }
    public ProductItemView(Context context) {
        super(context);
        this.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));

    }

    public void bind(Product product){
        textNameProduct.setText(product.getNameProduct());
        textQuantity.setText(String.valueOf(product.getQuantity()));
        textPriceProduct.setText(product.getPriceProduct());
        Glide.with(imageProduct.getContext()).load(product.getUrlPhoto()).into(imageProduct);
    }

}