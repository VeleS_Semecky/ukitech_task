package com.example.ukietech.adapter.product;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.ViewGroup;

import com.example.ukietech.adapter.bean.ProviderBeanProduct;
import com.example.ukietech.adapter.bean.ProviderBeanShop;
import com.example.ukietech.adapter.bean.finder.ProductBeanFinder;
import com.example.ukietech.adapter.core.RecyclerViewAdapterBase;
import com.example.ukietech.adapter.core.ViewWrapper;
import com.example.ukietech.model.Product;
import com.example.ukietech.ui.activity.core.BaseActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@EBean
public class ProductAdapter extends RecyclerViewAdapterBase<Product, ProductItemView> {

    @RootContext
    BaseActivity baseActivity;

    @Bean
    ProductBeanFinder productBeanFinder;

    @AfterViews
    public void initAdapter() {
        DatabaseReference feedReference = FirebaseDatabase.getInstance().getReference().child("Products").orderByChild("Products").getRef();
        feedReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Product> listProduct = new ArrayList<>();
                listProduct.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Product product = snapshot.getValue(Product.class);
                    listProduct.add(product);
                }
                productBeanFinder.sortList(listProduct);
                setItem(listProduct);
                notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }



    @Override
    protected ProductItemView onCreateItemHolder(ViewGroup parent, int viewType) {
        return ProductItemView_.build(baseActivity);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewWrapper<ProductItemView> holder, int position) {
        ProductItemView view = holder.getView();
        Product product = getItem().get(position);
        view.setTag(product);
        view.bind(product);
    }
}