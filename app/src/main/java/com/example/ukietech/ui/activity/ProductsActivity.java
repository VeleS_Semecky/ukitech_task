package com.example.ukietech.ui.activity;

import android.view.Window;

import com.example.ukietech.R;
import com.example.ukietech.ui.activity.core.BaseActivity;
import com.example.ukietech.ui.navigator.NavigatorManager;
import com.example.ukietech.ui.navigator.core.ResourceManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.WindowFeature;

@WindowFeature({Window.FEATURE_NO_TITLE})
@EActivity(R.layout.activity_main)
public class ProductsActivity extends BaseActivity {

    @Bean
    public NavigatorManager navigatorManager;

    @AfterViews
    public void initView(){
//        navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.ADDNEWSHOP_FRAGMENT);
        navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.LIST_PRODUCT_FRAGMENT);
//        navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.NULL_FRAGMENT);
    }
}