package com.example.ukietech.ui.fragment.core;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.example.ukietech.R;
import com.example.ukietech.ui.activity.core.BaseActivity;
import com.example.ukietech.ui.activity.core.OnBackPressedListener;
import com.example.ukietech.ui.navigator.NavigatorManager;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import io.reactivex.functions.Consumer;

/**
 * Created by Юрий on 24.03.2018.
 */
@EFragment
public abstract class BaseFragment extends Fragment implements OnBackPressedListener{

    @ViewById(R.id.toolbar)
    protected Toolbar toolbar;

    @ViewById(R.id.toolbarContainer)
    protected RelativeLayout toolbarLayout;

    public RelativeLayout getToolbarLayout() {
        return toolbarLayout;
    }

    private Consumer<String> onNextAction = s ->backPressed();


    @Override
    public void backPressed() {
        getActivity().finish();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onAttachToActivity();
    }

    protected void onAttachToActivity(){
        getBaseActivity().setBackPressedListener(onNextAction);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onAttachToActivity();
    }

    protected BaseActivity getBaseActivity(){
        return (BaseActivity) getActivity();
    }

    @Override
    public void onDetach() {
        getBaseActivity().removeBackPressedListener(onNextAction);
        super.onDetach();
    }

    @Bean
    protected NavigatorManager navigatorManager;

    public void clickToolbar(int id){
        Toast.makeText(getActivity(),R.string.app_name,Toast.LENGTH_SHORT).show();
    }
}
