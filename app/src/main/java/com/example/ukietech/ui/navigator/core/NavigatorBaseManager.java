package com.example.ukietech.ui.navigator.core;



import com.example.ukietech.ui.activity.core.BaseActivity;
import com.example.ukietech.ui.fragment.core.BaseFragment;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by Юрий on 24.03.2018.
 */
@EBean
public abstract class NavigatorBaseManager {

    @RootContext
    protected BaseActivity baseActivity;

    public abstract Manager getMainManager(BaseFragment baseFragment);

}
