package com.example.ukietech.ui.navigator.core;

/**
 * Created by Юрий on 24.03.2018.
 */

public class ResourceManager {
    public static final class ActivityId{
        //1-20
        public final static int START_ACTIVITY = 1;
        public final static int SHOP_ACTIVITY = 2;
        public final static int PRODUCT_ACTIVITY = 3;

    }

    public static final class FragmentId{
        //100-500
        public final static int FIRST_FRAGMENT = 100;
        public final static int AUTH_FRAGMENT = 101;
        public final static int NULL_FRAGMENT = 102;
        public final static int LIST_PRODUCT_FRAGMENT = 103;
        public final static int LIST_SHOPS_FRAGMENT = 104;
        public final static int ADD_NEW_SHOP_FRAGMENT = 105;

    }

    public static final class ToolbarId{
        //1000 - 1500
        public final static int SIMPLE = 1000;
        public final static int YES_OR_NO = 1001;
        public final static int PRODUCT = 1002;

    }
}
