package com.example.ukietech.ui.fragment;

import com.example.ukietech.R;
import com.example.ukietech.ui.fragment.core.BaseFragment;
import com.example.ukietech.ui.navigator.core.ResourceManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

/**
 * Created by Юрий on 26.03.2018.
 */
@EFragment(R.layout.fragment_null)
public class FragmentToolbarButContentNull extends BaseFragment {
    @AfterViews
    public void initToolbar(){
        navigatorManager.getMainManager(this).initToolbar(ResourceManager.ToolbarId.SIMPLE, R.string.app_name);
    }
}
