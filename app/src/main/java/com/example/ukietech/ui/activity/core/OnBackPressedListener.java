package com.example.ukietech.ui.activity.core;

/**
 * Created by Юрий on 20.03.2018.
 */

public interface OnBackPressedListener {

    void backPressed();
}
