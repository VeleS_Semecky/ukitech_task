package com.example.ukietech.ui.navigator;


import com.example.ukietech.ui.fragment.core.BaseFragment;
import com.example.ukietech.ui.navigator.core.Manager;
import com.example.ukietech.ui.navigator.core.NavigatorBaseManager;

import org.androidannotations.annotations.EBean;

/**
 * Created by Юрий on 24.03.2018.
 */
@EBean
public class NavigatorManager extends NavigatorBaseManager {

    @Override
    public Manager getMainManager(BaseFragment baseFragment) {

        return new ManagerMain(baseFragment,baseActivity);

    }

}
