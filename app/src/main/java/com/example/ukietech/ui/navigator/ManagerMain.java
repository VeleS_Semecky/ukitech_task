package com.example.ukietech.ui.navigator;


import android.support.annotation.NonNull;

import com.example.ukietech.R;
import com.example.ukietech.ui.activity.core.BaseActivity;
import com.example.ukietech.ui.fragment.AddNewShopFragment_;
import com.example.ukietech.ui.fragment.AuthContainerFragment_;
import com.example.ukietech.ui.fragment.FirstFragment_;
import com.example.ukietech.ui.fragment.FragmentToolbarButContentNull_;
import com.example.ukietech.ui.fragment.ListProductsFragment_;
import com.example.ukietech.ui.fragment.ListShopsFragment_;
import com.example.ukietech.ui.fragment.core.BaseFragment;
import com.example.ukietech.ui.navigator.core.BaseManager;
import com.example.ukietech.ui.navigator.core.ResourceManager;
import com.example.ukietech.ui.navigator.core.ResourceNames;

import org.androidannotations.api.builder.FragmentBuilder;

/**
 * Created by Юрий on 24.03.2018.
 */

public class ManagerMain extends BaseManager {

    ManagerMain(BaseFragment baseFragment, BaseActivity baseActivity) {
        super(baseFragment, baseActivity);
    }

    @Override
    public void moveFragmentTo(int id, Object... o) {
        switch (id){
            case ResourceManager.FragmentId.FIRST_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().replace(R.id.fragment_container, FirstFragment_.builder().build(), ResourceNames.FIRST_FRAGMENT).commit();
                break;
            case ResourceManager.FragmentId.AUTH_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().replace(R.id.containerAuth, AuthContainerFragment_.builder().build(), ResourceNames.AUTH_FRAGMENT).commit();
                break;
            case ResourceManager.FragmentId.NULL_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().replace(R.id.containerToolbar, FragmentToolbarButContentNull_.builder().build(), ResourceNames.NULL_FRAGMENT).commit();
                break;
            case ResourceManager.FragmentId.ADD_NEW_SHOP_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().replace(R.id.fragment_container, AddNewShopFragment_.builder().build(), ResourceNames.ADD_NEW_SHOP_FRAGMENT).commit();
                break;
            case ResourceManager.FragmentId.LIST_SHOPS_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().replace(R.id.fragment_container, ListShopsFragment_.builder().build(), ResourceNames.LIST_SHOPS_FRAGMENT).commit();
                break;
            case ResourceManager.FragmentId.LIST_PRODUCT_FRAGMENT:
                replaceFragment(R.id.fragment_container, ListProductsFragment_.builder(),o[0].toString(),ResourceNames.LIST_PRODUCTS_FRAGMENT);
                break;

        }
    }

    private void replaceFragment(int layoutFragment, FragmentBuilder fragmentBuilder, @NonNull String bundle, String resourceName){
        baseActivity.getFragmentManager().beginTransaction().replace(layoutFragment, (android.app.Fragment) fragmentBuilder.arg("bundle",bundle).build(), resourceName).commit();
    }

    private void replaceFragment(int layoutFragment, FragmentBuilder fragmentBuilder, int bundle, String resourceName){
        baseActivity.getFragmentManager().beginTransaction().replace(layoutFragment, (android.app.Fragment) fragmentBuilder.arg("bundle",bundle).build(), resourceName).commit();
    }

    @Override
    public void removeFragment() {
            baseFragment.getActivity().getFragmentManager().beginTransaction().remove(baseFragment).commit();
    }
}
