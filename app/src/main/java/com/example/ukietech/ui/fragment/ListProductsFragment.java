package com.example.ukietech.ui.fragment;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.ukietech.R;
import com.example.ukietech.adapter.bean.ProviderBeanProduct;
import com.example.ukietech.model.Product;
import com.example.ukietech.other.QrOrientationLibsCaptureActivity;
import com.example.ukietech.ui.fragment.core.BaseFragment;
import com.example.ukietech.ui.navigator.core.ResourceManager;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Юрий on 26.03.2018.
 */
@EFragment(R.layout.fragment_products)
public class ListProductsFragment extends BaseFragment {

    public final int CUSTOMIZED_REQUEST_CODE = 0x0000ffff;
    private List<Product> productList = new ArrayList<>();

    @Bean
    ProviderBeanProduct providerBeanProduct;

    @ViewById(R.id.recycler_view)
    RecyclerView recyclerView;

    @AfterViews
    public void initToolbar(){
        Bundle bundle = getArguments();
        if (bundle != null) {
            String nameToolbar = bundle.getString("bundle");
            navigatorManager.getMainManager(this).initToolbar(ResourceManager.ToolbarId.PRODUCT, nameToolbar);
        }
    }

    @Override
    public void clickToolbar(int id) {
        switch (id){
            case R.id.btnBack:
                goToFragment();
                break;
        }
    }

    @Override
    public void backPressed() {
        goToFragment();
    }

    private void goToFragment(){
        navigatorManager.getMainManager(this).moveFragmentTo(ResourceManager.FragmentId.LIST_SHOPS_FRAGMENT);
    }

    @Click(R.id.fab_add_product)
    public void onAddProduct(){
        IntentIntegrator.
                forFragment(this).
                setCaptureActivity(QrOrientationLibsCaptureActivity.class).
                setOrientationLocked(false).
                setRequestCode(CUSTOMIZED_REQUEST_CODE).
                initiateScan();
    }

    @OnActivityResult(CUSTOMIZED_REQUEST_CODE)
    void onResult(int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(resultCode, data);
        if(result.getContents() == null) {
            Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_LONG).show();
        } else {
             addNewProduct();
             providerBeanProduct.addNewProduct();
            Toast.makeText(getActivity(), "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
        }
    }

    private void addNewProduct(){
        Product product = new Product();
        DatabaseReference myRef;
        String SHOPS_ROOT = "Products";
        myRef = FirebaseDatabase.getInstance().getReference().child(SHOPS_ROOT);
        String key = myRef.push().getKey();
        Uri imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                "://" + getResources().getResourcePackageName(R.drawable.qaz)
                + '/' + getResources().getResourceTypeName(R.drawable.qaz)
                + '/' + getResources().getResourceEntryName(R.drawable.qaz));
        StorageReference reference = FirebaseStorage.getInstance().getReference().child("image_feed/" + Calendar.getInstance().getTimeInMillis() + ".jpg");
        product.setNameProduct("Pepsi");
        product.setPriceProduct("$14");
        product.setQuantity(12);
        reference.putFile(imageUri).addOnSuccessListener(taskSnapshot -> {
            Uri fullSizeUrl = taskSnapshot.getDownloadUrl();
            assert fullSizeUrl != null;
            product.setUrlPhoto(fullSizeUrl.toString());
            myRef.child(key).setValue(product); });

    }


}
