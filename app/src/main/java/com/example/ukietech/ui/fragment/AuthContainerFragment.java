package com.example.ukietech.ui.fragment;

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.ukietech.R;

import com.example.ukietech.ui.activity.ShopsActivity_;
import com.example.ukietech.ui.fragment.core.BaseFragment;
import com.example.ukietech.ui.navigator.core.ResourceManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OnActivityResult;

/**
 * Created by Юрий on 26.03.2018.
 */
@EFragment(R.layout.fragment_auth)
public class AuthContainerFragment extends BaseFragment{
    private FirebaseAuth mAuth;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 103;
    @AfterViews
    public void GoogleSingIn() {
        // Initialize authentication and set up callbacks
        mAuth = FirebaseAuth.getInstance(); 

        // GoogleApiClient with Sign In
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Auth.GOOGLE_SIGN_IN_API,
                        new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                .requestEmail()
                                .requestIdToken(getString(R.string.default_web_client_id))
                                .build())
                .build();

    }

    @Click(R.id.buttonAuthAdmin)
    public void onClickAuthAdmin(){
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(intent, ResourceManager.FragmentId.ADD_NEW_SHOP_FRAGMENT);
    }

    @Click(R.id.buttonAuthUser)
    public void onClickAuthUser(){
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(intent, ResourceManager.FragmentId.ADD_NEW_SHOP_FRAGMENT);
    }

    @OnActivityResult(ResourceManager.FragmentId.ADD_NEW_SHOP_FRAGMENT)
    public void singInGoogle(int resultCode, Intent data){
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);

            } else {
                // Google Sign In failed, update UI appropriately
                // ...
            }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d("sad", "firebaseAuthWithGoogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), task -> {
                    Log.d("asd", "signInWithCredential:onComplete:" + task.isSuccessful());
                    // If sign in fails, display a message to the user. If sign in succeeds
                    // the auth state listener will be notified and logic to handle the
                    // signed in user can be handled in the listener.
                    if (!task.isSuccessful()) {
                        Log.w("asd", "signInWithCredential", task.getException());

                    } else {
                        Toast.makeText(getActivity(), "Scanned: " , Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getActivity(), ShopsActivity_.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                });
    }

}
