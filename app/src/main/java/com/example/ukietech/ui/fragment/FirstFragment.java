package com.example.ukietech.ui.fragment;

import android.widget.ImageView;
import android.widget.Toast;


import com.example.ukietech.R;
import com.example.ukietech.ui.fragment.core.BaseFragment;
import com.example.ukietech.ui.navigator.core.ResourceManager;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Юрий on 24.03.2018.
 */
@EFragment(R.layout.fragment_first)
public class FirstFragment extends BaseFragment {
    @ViewById(R.id.imageView)
    ImageView image;

    @AfterViews
    public void initToolbar(){
        navigatorManager.getMainManager(this).initToolbar(ResourceManager.ToolbarId.SIMPLE, String.valueOf(R.string.app_name));
        setImage(image);
    }

    @Override
    public void backPressed() {
        Toast.makeText(getBaseActivity(),"12e",Toast.LENGTH_SHORT).show();
    }



    public void setImage(ImageView image){
        Picasso.get()
                .load("http://content.silpo.ua/uploads/2018/03/22/5ab368005023a.png")
                .into(image);
    }


}
