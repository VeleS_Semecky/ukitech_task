package com.example.ukietech.ui.fragment;

import com.example.ukietech.R;
import com.example.ukietech.adapter.bean.ProviderBeanShop;
import com.example.ukietech.model.Shop;
import com.example.ukietech.ui.fragment.core.BaseFragment;
import com.example.ukietech.ui.navigator.core.ResourceManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

import java.util.ArrayList;

/**
 * Created by Юрий on 26.03.2018.
 */
@EFragment(R.layout.fragment_shops)
public class ListShopsFragment extends BaseFragment{

    @Bean
    ProviderBeanShop providerBeanShop;



    @Click(R.id.fab_add_shop)
    public void addNewShop(){
        navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.ADD_NEW_SHOP_FRAGMENT);
    }

}
